aspectc++ (1:2.3+git20221129-2) unstable; urgency=medium

  * debian/control: Build-Depend on libpolly-14-dev, Closes: #1028810

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 29 Jan 2023 14:54:50 -0500

aspectc++ (1:2.3+git20221129-1) unstable; urgency=medium

  * New upstream version
  * Build against llvm-14, closes: #1012184, #1004884, #1012897

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 29 Nov 2022 17:54:11 -0500

aspectc++ (1:2.3+git20211104-2) unstable; urgency=medium

  * Update float128.patch, kudost oFrédéric Bonnard <frediz@debian.org>
    for the patch!

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 22 Nov 2021 15:49:20 -0500

aspectc++ (1:2.3+git20211104-1) unstable; urgency=medium

  * New upstream version
  * Compile against clang-12, Closes: #997111

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 04 Nov 2021 11:30:14 -0400

aspectc++ (1:2.3-4) unstable; urgency=medium

  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 28 Feb 2021 09:49:38 -0500

aspectc++ (1:2.3-3) experimental; urgency=medium

  * Re-enable the float128 patch

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 21 Feb 2021 14:34:04 -0500

aspectc++ (1:2.3-2) experimental; urgency=medium

  * Add patch from upstream that fixes 32bit segfault
  * Disable clang-gcc14-workaround and float128 patches

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 21 Feb 2021 08:32:46 -0500

aspectc++ (1:2.3-1) unstable; urgency=medium

  * New upstream version 2.3
    - only cleanups and important Clang 11 related fixes compared to
      previous snapshot
  * drop pkgconfig.patch, merged upstream

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 17 Feb 2021 20:10:30 -0500

aspectc++ (1:2.2+git20210209-1) unstable; urgency=high

  * New upstream version 2.2+git20210209
  * Build against Clang 11, closes: #974796, #974812

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 09 Feb 2021 22:37:09 -0500

aspectc++ (1:2.2+git20200405-1) unstable; urgency=medium

  * New upstream version 2.2+git20200405
    - upstream applied suggested patch to build on armel
      Closes: #953540
  * Add patch clang-9.0.1 partially back

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 11 Apr 2020 18:20:07 -0400

aspectc++ (1:2.2+git20200229-1) unstable; urgency=medium

  * New Upstream version
  * Build against Clang 9.0.1, Closes: #912786

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 01 Mar 2020 10:09:17 -0500

aspectc++ (1:2.2+git20181008-4) unstable; urgency=medium

  * Rename autopkgtest
  * Bump standards version
  * bump debhelper to compat level 10
  * Use pkg-config to locate libxml2 (Closes: #949053)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 19 Jan 2020 11:22:24 -0500

aspectc++ (1:2.2+git20181008-2) unstable; urgency=medium

  * Avoid using __float128 types, fixes FTBFS on ppc64el

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 23 Oct 2018 08:08:17 -0400

aspectc++ (1:2.2+git20181008-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Reinhard Tartler ]
  * New Upstream version.
  * Fix building with and build against Clang 6.0 with gcc-8.
    (Closes: #902220)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 21 Oct 2018 10:05:31 -0400

aspectc++ (1:2.2+git20170823-8) unstable; urgency=medium

  * Revert "Build depend on llvm-dev (Closes: #902220)" (Closes: #906972)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 26 Aug 2018 10:17:28 -0400

aspectc++ (1:2.2+git20170823-7) unstable; urgency=medium

  * Build depend on llvm-dev (Closes: #902220)
  * Add autopkgtest

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 25 Jun 2018 10:59:10 -0400

aspectc++ (1:2.2+git20170823-6) unstable; urgency=medium

  * Add patch to fix FTBFS with gcc-8 (Closes: #897709)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 22 May 2018 09:31:45 -0400

aspectc++ (1:2.2+git20170823-5) unstable; urgency=medium

  * Fix typo that resulted in wrong CFLAGS during compilation

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 18 Apr 2018 19:14:12 -0400

aspectc++ (1:2.2+git20170823-4) unstable; urgency=medium

  * Fix compilation options for builds on armel and armhf

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 17 Apr 2018 20:59:46 -0400

aspectc++ (1:2.2+git20170823-3) unstable; urgency=medium

  * debian/control: Update Vcs fields to point to salsa
  * aspectc++ FTBFS on armel: decrease optimization levels like on armhf
    (Closes: #894620)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 15 Apr 2018 17:48:31 -0400

aspectc++ (1:2.2+git20170823-2) unstable; urgency=medium

  * Use the default system clang (Closes: #893404)
    - Build depend on libclang-dev, which is build from llvm-defaults
      and is bumped by the llvm maintainers to point to the default
      version of clang

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 19 Mar 2018 09:04:07 -0400

aspectc++ (1:2.2+git20170823-1) unstable; urgency=medium

  * New upstream release
  * Fixes compilation with gcc-7 (Closes: #853319)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 22 Aug 2017 21:37:27 -0400

aspectc++ (1:2.2-1) experimental; urgency=medium

  [ Reinhard Tartler ]
  * Upload to debian/experimental

  [ Radovan Birdic ]
  * Compile aspectc++ with -fPIC on mipsen (Closes: #845535)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 27 Mar 2017 20:31:11 -0400

aspectc++ (1:2.1-2) unstable; urgency=medium

  * Team upload (collab-maint)
  * Add VCS browser and fix VCS fields
  * Bump std-version to 3.9.8
  * Bump compat level to 9
  * Switch to llvm 3.8 (Closes: #836606)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 07 Oct 2016 10:50:23 +0200

aspectc++ (1:2.1-1) unstable; urgency=medium

  * Import Upstream version 2.1

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 20 Aug 2016 12:27:01 -0400

aspectc++ (1:2.0+svn20160531-2) unstable; urgency=medium

  * Compile with -O1 on armhf to avoid FTBFS on armhf

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 17 Jun 2016 22:23:23 -0400

aspectc++ (1:2.0+svn20160531-1) unstable; urgency=medium

  * Imported Upstream version 2.0+svn20160531
    - Fixes FTBFS on ppc64el (Closes: #824033)
    - Fixes FTBFS on arm64 (Closes: #802819)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 30 May 2016 18:16:12 -0400

aspectc++ (1:2.0+svn20160329-1) unstable; urgency=medium

  * Imported Upstream version 2.0+svn20160329

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 28 Mar 2016 23:26:18 -0400

aspectc++ (1:2.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 04 Mar 2016 09:36:24 -0500

aspectc++ (1:2.0-1) experimental; urgency=medium

  * Imported Upstream version 2.0
  * Bump standard version, no changes needed

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 28 Feb 2016 10:27:33 -0500

aspectc++ (1:1.2+svn20151025-1) unstable; urgency=medium

  * New upstream version
  * Enable LLVM support, Closes: #754697

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 24 Aug 2015 20:42:29 -0400

aspectc++ (1:1.2+svn20150823-1) unstable; urgency=medium

  * New upstream snapshot
  * Compiles with GCC-5, Closes: #777781
  * Bump standard version, no changes needed
  * This package is now maintained on git.debian.org

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 23 Aug 2015 22:04:29 -0400

aspectc++ (1:1.2-1) unstable; urgency=low

  * New upstream release.
  * Workaround for building with g++-4.8 - aspectc++ supports __int128 by
    default only when woven on 64bits, which the upstream provided sources
    don't. closes: #701244

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 13 Oct 2013 16:12:00 -0400

aspectc++ (1:1.1+svn20120529-2) unstable; urgency=low

  * Install missing lexertl files in libpuma-dev Puma.
    Thanks to Christoph Egger <christoph@debian.org> for reporting
    Closes: #679583

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 30 Jun 2012 09:45:17 +0200

aspectc++ (1:1.1+svn20120529-1) unstable; urgency=low

  * New upstream snapshot.
    - Fixes with fcntl handling on kFreeBSD
    - Upstream fixes in the Puma Parser

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 30 May 2012 10:01:33 +0200

aspectc++ (1:1.1+svn742-1) unstable; urgency=low

  * New upstream snapshot.
    - fixes compilation with gcc 4.7 (Closes: #672013)
  * disable parallelism inside debian/rules (but not submakes)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 22 May 2012 21:12:18 +0200

aspectc++ (1:1.1-1) unstable; urgency=low

  * New upstream release.
  * Apply patches to fix FTBFS with recent gcc versions. Closes: #667103
  * Avoid empty directory usr/sbin (found by lintian)
  * Bump Standards version to 3.9.3.1 (no changes needed)
  * Avoid running doxygen on the buildds.

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 11 Apr 2012 21:09:34 +0200

aspectc++ (1:1.0-4) unstable; urgency=low

  * debian/rules: Build doxygen in binary-arch target until buildds call the
    build-arch target instead of build to avoid FTBFS.

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 30 Jun 2011 21:11:23 +0200

aspectc++ (1:1.0-3) unstable; urgency=low

  * Don't compress examples files
  * Install into correct paths
  * add notes for linking against libPuma.a
  * Don't install non-functional Makefiles in example directories
  * Drop -no-rtti and from CFLAGS
  * implement build-arch and build-indep target as suggested by debian policy
  * Honor 'nocheck' flag in DEB_BUILD_OPTIONS

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 30 Jun 2011 17:29:15 +0200

aspectc++ (1:1.0-2) unstable; urgency=low

  * apply patch from upstream to fix FTBFS on kFreeBSD
  * simplify Vcs-Bzr field

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 19 Apr 2011 21:59:07 +0200

aspectc++ (1:1.0-1) unstable; urgency=low

  * New upstream version
  * don't ignore errors in maintainer scripts
  * aspectc++.postinst: don't call ag++ with full path
  * prefer dh_prep over dh_clean -k
  * Add ${misc:Depends} dependencies
  * support parallel builds
  * bump standards version
  * bump debhelper compat level to 7
  * install all Puma samples
  * switch to source format 3.0 (quilt)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 18 Apr 2011 21:40:56 +0200

aspectc++ (1.0pre4~svn.20080711-1) unstable; urgency=low

  * new upstream snapshot.
  * include all upstream documentation. Clarifying emails regarding
    licensing has been included into debian/copyright.
  * reformat description following recomendations of
    http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-Description
    (Closes: #480316)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 07 Jul 2008 14:41:02 +0200

aspectc++ (1.0pre4~svn.20080409+dfsg-3) unstable; urgency=low

  * Fix another missing include, this time in Ag++/StdSystem.cc

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 10 Apr 2008 17:40:52 +0200

aspectc++ (1.0pre4~svn.20080409+dfsg-2) unstable; urgency=low

  * After another round of reviewing Bens diff, the NMU was correct after
    all.  Incorporating the Ppatch from
    http://bugs.debian.org/cgi-bin/bugreport.cgi?msg=23;bug=417489
    Really Closes: #417489 this time.
  * reweave Puma with changes from previous change.
  * More cleanups in the Puma buildscripts: debian/rules honors now noopt in
    the variable DEB_BUILD_OPTIONS.
  * add Vcs-Bzr headers to debian/control.

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 10 Apr 2008 13:37:36 +0200

aspectc++ (1.0pre4~svn.20080409+dfsg-1) unstable; urgency=low

  * new upstream snapshot
    - now builds properly with GCC 4.3 (Closes: #417489, #424106)
    - uses bash in the ag++ testsuite (Closes: #373665)
    - explicitly not ACKing NMU from Ben Hutchings. Upstream has fixed
      the issues with GCC 4.3 independently
    - Now shipping prewoven puma sources directly in the orig.tar.gz (again)
    - remove unlicensed documentation.
  * bumbed standards version
  * use CURDIR instead of PWD, thanks lintian
  * updated maintainer record
  * add script and target for retrieving a clean upstream tarball
  * Incorporating and Acking NMU from Sandro Tosi. (Closes: #424106)
  * compile ac++ twice: one time with prewoven Puma, then reweave a copy
    of Puma, then build a new ac++ with the freshly woven Puma. This
    ensures that ac++ is actually able to compile itself (Closes: #474560)
  * ensure that the AspectC++ testsuite is using /bin/bash (Closes: #373665)
  * compile with -O2 instead of -O3. -O2 is likely to cause problems on
    non-mainstream architectures. Plus, add '-g' to compilation flags, so
    that a build with DEB_BUILD_OPTIONS='noopt' actually creates a package
    with debugging information.
  * add dependency on gsfonts for having proper fonts in the doxygen
    outputs.
  * Bump Standard version to 3.7.3 (no changes needed).
  * Use Homepage Field.
  * Move packages into correct sections: libpuma-doc -> doc,
    libpuma-dev -> libdevel.
  * simplify debian/rules a bit.
  * install (woven) Puma development headers into libpuma-dev.

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 10 Apr 2008 09:23:51 +0200

aspectc++ (0.99+1.0pre3-3.1) unstable; urgency=low

  * Non-maintainer upload

  [ Sandro Tosi ]
  * debian/rules
    - added removal of binary not cleaned (Closes: #424106)

  [ Ben Hutchings ]
  * Fixed recursive make invocations to avoid hiding failures
  * Added/changed #include and using directives for g++ 4.3
    (Closes: #417489) (partly by Martin Michlmayr)
  * Added missing parentheses in Puma::FilenameInfo::name()

 -- Ben Hutchings <ben@decadent.org.uk>  Sun, 06 Apr 2008 01:15:28 +0100

aspectc++ (0.99+1.0pre3-3) experimental; urgency=low

  * more cleanups in Puma/Makefile
  * removed Antonio from uploaders on his request. Thank you for your work!
  * bumped standards version to 3.7.2 (no changes needed)
  * Introduce new package: libpuma-dev, for direct use of the
    puma library (Closes: #369771)
  * Introduce new package: libpuma-doc, with updated doxygen.conf
    containing 2 examples (ccparser and cparser) and doxygen generated docs
  * Added circular build-depend on itself, testing if the ac++ binary is
    working on all architectures!
  * Merge patches from upstream for ag++, fixing command line handling and
    fixes in the testsuite.

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 12 Jun 2006 10:53:28 +0200

aspectc++ (0.99+1.0pre3-2ubuntu2) gutsy; urgency=low

  * Rebuild to deal with launchpad looping.

 -- LaMont Jones <lamont@ubuntu.com>  Thu, 11 Oct 2007 21:28:50 -0600

aspectc++ (0.99+1.0pre3-2ubuntu1) feisty; urgency=low

  * Rebuild for ldbl128 change (powerpc, sparc).
  * Set Ubuntu maintainer address.

 -- Matthias Klose <doko@ubuntu.com>  Thu,  1 Mar 2007 22:30:19 +0000

aspectc++ (0.99+1.0pre3-2) unstable; urgency=low

  * Fix some more cleanup() functions, which can go frenzy on at least amd64
    (applied upstread)
  * Fix description, thanks to Simon Waters (Closes: #362738)
  * reweave Puma to make the patch enabling the build with gcc-4.1 actually
    active (Closes: #357901, this time for real)

 -- Reinhard Tartler <siretart@tauware.de>  Sat,  3 Jun 2006 13:14:38 +0200

aspectc++ (0.99+1.0pre3-1) unstable; urgency=low

  * new upstream release
  * Apply patch from Martin Michlmayr for g++-4.1 (Closes: #357901)
  * further (simple) patches in Puma/ and AspectC++ for g++-4.1
  * note that Puma needs to be rewoven so that it can be compiled
    with g++-4.1. This will be done we switch the default compiler
    version.
  * Patch JoinPointRepo.cc so that it doesn't loop endlessly anymore.

 -- Reinhard Tartler <siretart@tauware.de>  Fri,  7 Apr 2006 11:56:35 +0200

aspectc++ (0.99+1.0pre2-4) unstable; urgency=low

  * Make clean target less noisy
  * Compile with -O2. This fixes many segfaults on different archs
    (Closes: #349090)
  * fix Puma/tools/Makefile to make tools-clean possible at all!
  * do not clean examples, that's broken anyway
  * some compile fixes for segfaults on 64 mode architectures.
    I had to reweave Puma for these fixes, thats the reason for the
    bloat in .diff.gz (Closes: #345568)
  * The tests for ag++ need a working ac++ in PATH. ensure this in
    debian/rules
  * Install the examples from AspectC++/examples

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 29 Jan 2006 18:38:54 +0100

aspectc++ (0.99+1.0pre2-3) unstable; urgency=low

  * polish debian/copyright
  * set PUMA_CONFIG in debian/rules for running the testsuite
  * take some Makefiles from upstream CVS for "optimized" linking
    (suggestion as per suggestion from from upstream)
  * remove the script update-puma.config. Use dpkg-reconfigure to regenerate
    the puma.config file

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 10 Jan 2006 14:09:14 +0100

aspectc++ (0.99+1.0pre2-2) unstable; urgency=low

  * Don't build shared Puma at all, we link to it statically after all, and no
    shared lib is exposed anyway, since Puma is way too unstable and no other
    application is using Puma at the moment. (Closes: #345542)
  * Fix wrong cast in a debug function. This is quite hackish, and Puma on 64
    bit archs is experimental anyway. I'm working with upstream for real 64bit
    support (Closes: #345541)

 -- Reinhard Tartler <siretart@tauware.de>  Sun,  1 Jan 2006 22:11:45 +0100

aspectc++ (0.99+1.0pre2-1) unstable; urgency=low

  * New upstream release. (Closes: #277538)

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 23 Dec 2005 10:49:40 +0100

aspectc++ (0.99+1.0pre1-1) unstable; urgency=low

  * installed ac++ and ag++ shellskript wrappers
    for setting PUMA_CONFIG
  * introduced update-puma.config shellscript to regenerate
    /etc/puma.config
  * move aspectc++ binaries to /usr/lib/aspectc++

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 21 Dec 2005 11:52:30 +0100
