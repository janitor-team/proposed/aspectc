#!/bin/sh
#
#  Script to prepare the aspectc++ package for distribution.
#  Copyright (C) 2008, Reinhard Tartler
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# this script essentially does 2 things:
#  - weave Puma (so that aspectc++ is not necessary for building)
#  - strip of unlicensed material, mainly documentation

set -e

# executed in the root directory of the unapacked aspectc++ source tree

# weave Puma, requires installed package aspectc++
make -C Puma weave
make -C Puma tools-clean docs-clean examples-clean libclean
