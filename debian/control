Source: aspectc++
Section: devel
Priority: optional
Maintainer: Reinhard Tartler <siretart@tauware.de>
Build-Depends: debhelper (>= 10), pkg-config, libxml2-dev, docbook-to-man, zlib1g-dev, libedit-dev, llvm-14-dev, libclang-14-dev, libpolly-14-dev
Build-Depends-Indep: doxygen, graphviz, gsfonts
Standards-Version: 4.4.1
Homepage: http://www.aspectc.org
Vcs-Git: https://salsa.debian.org/debian/aspectc
Vcs-Browser: https://salsa.debian.org/debian/aspectc

Package: aspectc++
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, g++
Description: aspect-oriented programming extension for C++
 AspectC++ supports Aspect-Oriented Programming with C++, by providing:
 .
  - an aspect language extension to C++.
  - an aspect weaver that does source-to-source translation.
 .
 AspectC++ provides for C++ similar features like its best known
 cousin, AspectJ, provides for Java.

Package: libpuma-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends}
Suggests: libpuma-doc
Description: C/C++/AspectC++ Scanner and Parsers
 libPuma is a library written in C++ for Scanning and Parsing C++
 Code. It also does some semantic analysis.
 .
 It was written to facilitate the development for tools, which manipulate
 and/or transfor C/C++ Code.
 .
 This package ships a static copy of libPuma only.

Package: libpuma-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: C/C++/AspectC++ Scanner and Parsers
 libPuma is a library written in C++ for Scanning and Parsing C++
 Code. It also does some semantic analysis.
 .
 It was written to facilitate the development for tools, which manipulate
 and/or transfor C/C++ Code.
 .
 This package ships doxygen generated documentation for Puma.
