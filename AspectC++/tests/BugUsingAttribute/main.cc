// Fine if it compiles

// C++11 alias declaration allows attributes after the identifier
using name1 [[deprecated]] = void;
using name2 __attribute__((deprecated)) = void;

// GNU extension: allow attributes in using directive
// See https://gcc.gnu.org/onlinedocs/gcc-6.5.0/gcc/Namespace-Association.html
namespace ns {}
using namespace ns __attribute((strong));

int main() {}
