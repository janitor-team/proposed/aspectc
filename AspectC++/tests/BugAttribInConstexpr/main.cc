// Fine if it compiles

constexpr
int fun(int i) {
    switch (i) {
        case 1:
            [[fallthrough]] ;
        case 2:
            return 1;
    }
    return 0;
}

int main() {}
