#!/bin/sh
#
#  Script to create a 'pristine' tarball for the aspectc++ source package.
#  based on a similar script for the debian ffmpeg source package.
#  Copyright (C) 2008, Reinhard Tartler
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -eu

usage() {
	cat >&2 <<EOF
usage: $0 <options>
  -h : display help
  -r : svn revision
  -c : path to cleanup script
  -o : output tarball name
  -d : date in svn compatible format (e.g. 20071224)
EOF
}

debug () {
	$DEBUG && echo "DEBUG: $*" >&2
}

error () {
	echo "$1" >&2
	exit 1;
}

set +e
PARAMS=`getopt hr:c:o:d: "$@"`
if test $? -ne 0; then usage; exit 1; fi;
set -e

eval set -- "$PARAMS"

DEBUG=false
REVISION=
CLEANUPSCRIPT=
TARBALL=
SVNDATE=

while test $# -gt 0
do
	case $1 in
		-h) usage; exit 1 ;;
		-r) REVISION=$2; shift ;;
		-c) CLEANUPSCRIPT=$2; shift ;;
		-o) TARBALL=$2; shift ;;
		-d) SVNDATE=$2; shift ;;
		--) shift ; break ;;
		*)  echo "Internal error!" ; exit 1 ;;
	esac
	shift
done

# dammit 'basic' regular expressions :/
if [ `expr match $SVNDATE '[0-9][0-9]*$'` -eq 0 ]; then
	error "svndate $SVNDATE is not a plain revision number"
fi

REVISION="{${SVNDATE}}"
BASEDIRNAME="aspectc++.svn${SVNDATE}"
SVNURL="https://svn.aspectc.org/repos/AspectC++-Project/trunk"

if [ -z $TARBALL ]; then
	error "you need to specify a tarballname"
fi

if [ -n $CLEANUPSCRIPT ] && [ -f $CLEANUPSCRIPT ]; then
	if [ ! -x $CLEANUPSCRIPT ]; then
		error "$CLEANUPSCRIPT must be executable"
	fi
fi

TMPDIR=`mktemp -d`
trap 'rm -rf ${TMPDIR}'  EXIT

svn export -r${REVISION} --ignore-externals \
	$SVNURL \
	${TMPDIR}/${BASEDIRNAME}

# get svn externals
svn pg svn:externals $SVNURL | \
while read external url; do
	[ -z $url ] && continue
	dest="${TMPDIR}/${BASEDIRNAME}/${external}"
	svn export -r${REVISION} --ignore-externals $url $dest
done

if [ -n ${CLEANUPSCRIPT} ]; then
	( cd ${TMPDIR}/${BASEDIRNAME} && ${CLEANUPSCRIPT} )
fi

tar czf ${TARBALL} -C ${TMPDIR} ${BASEDIRNAME}

