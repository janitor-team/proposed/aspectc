// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2015  The 'ac++' developers (see aspectc.org)
//
// This program is free software;  you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA

#include "ClangFileTracker.h"
#include "clang/Frontend/CompilerInstance.h"

#include <iostream>
using namespace std;

using namespace clang;

ClangFileTracker::ClangFileTracker(ACConfig &config) :
    _config(config), _in_project(true), _ac_keywords_enabled(false) {
}

void ClangFileTracker::FileChanged (SourceLocation Loc, FileChangeReason Reason,
    SrcMgr::CharacteristicKind FileType, FileID PrevFID) {
  ACProject &project = _config.project();
  SourceManager &SM = project.get_compiler_instance()->getSourceManager();

  clang::PresumedLoc PL = SM.getPresumedLoc(Loc);
  llvm::StringRef Name = PL.getFilename();
  llvm::StringRef BufferName = SM.getBufferName(Loc);

  if (!(BufferName.startswith("<intro") ||
      (!Name.empty() && (Name.equals("<ac>") || Name.endswith("_virt"))))) {
    // recalculate state only if we have a real file change
    _in_project = project.isBelow(Name.str().c_str());
    _ac_keywords_enabled = _in_project && (_config.keywords() || Name.endswith(".ah"));
//    cout << "==> " << Name.str() << " " << BufferName.str() << " " << _in_project << " " << _ac_keywords_enabled << endl;
  }
}
