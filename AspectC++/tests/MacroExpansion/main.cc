// code reproduces the problem that caused bug 764, which was related
// to boost's BOOST_SCOPE_EXIT macros.

#include <stdio.h>
// #include <boost/scope_exit.hpp>

#define PP_CAT(a,b) a ## b

#define AUX(id)   \
    PP_CAT(boost_se_guard_, id)

#   define CLASS_END(id) \
  } AUX(id)(0);

int main() {
  printf ("MacroExpansion: test automatic macro expansion\n");
  printf ("==============================================\n");

  // int a = 0;
  // BOOST_SCOPE_EXIT(&a)
  // {
  //    a = 42;
  //    hello();
  // }
  // BOOST_SCOPE_EXIT_END
  struct A { A(int) { int rumpf; } float t;  CLASS_END(42)

  printf ("fine if it compiles\n");
  printf ("==============================================\n");
  return 0;
}

aspect NeededToTriggerTransformation {};
  
