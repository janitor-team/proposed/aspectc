<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Antonio</firstname>">
  <!ENTITY dhsurname   "<surname>Terceiro</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>10 February 2005</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>asaterceiro@inf.ufrgs.br</email>">
  <!ENTITY dhusername  "Antonio S. de A. Terceiro">
  <!ENTITY dhucpackage "<refentrytitle>AspectC++</refentrytitle>">
  <!ENTITY dhpackage   "aspectc++">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2005</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>ag++</refname>
    <refpurpose>a frontend to ac++ and g++</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>ag++</command>
        <arg choice="req"> -o <replaceable>output</replaceable> <replaceable>input.cpp</replaceable></arg>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>ag++</command>
        <arg choice="req"> -c -o <replaceable>output.o</replaceable> -o <replaceable>input.cpp</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  

  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>ag++</command> command.</para>

    <para>
      <command>ag++</command> is a frontend to ac++ and g++ provided by
      AspectC++.  For a better (and more
      complete) reference, see the AspectC++ documentation.
    </para>

    <para>
      <command>ag++</command>
       is called much like g++ itself. Check the two syntax forms above
       and follow these explanations:
    </para>

    <para>
      The first syntax form takes <emphasis>input.cpp</emphasis> and generates
      an executable named <emphasis>output</emphasis>, after weaving into
      input.cpp all the aspects that crosscut it.
      <emphasis>input.cpp</emphasis> has restrictions as it would have if we
      were compiling a regular C++ program: it must have an
      <emphasis>main()</emphasis> function, and everything you already know.
    </para>

    <para>
      The second syntax form takes <emphasis>input.cpp</emphasis> and generates
      an object file named <emphasis>output.o</emphasis>, after weaving into
      <emphasis>input.cpp</emphasis> all the aspects that crosscut it.
    </para>

    <para>
      <emphasis>Note:</emphasis> in both cases, <emphasis>input.cpp</emphasis>
      isn't actually changed. <command>g++</command> weaves to temporary files.
    </para>

    <para>
      Common <command>g++</command> arguments, like
      <emphasis>-I INDCIR</emphasis>,
      <emphasis>-L LIBCIR</emphasis>,
      <emphasis>-l libfoo</emphasis>, and so on,
      are recognized by <command>ag++</command> and rightly passed to
      <command>g++</command>.
    </para>

  </refsect1>
  
  <refsect1>
    <title>OPTIONS</title>

    <para>
    Further options can be seen in AspectC++ documentation.
    </para>
  </refsect1>

  <refsect1>
    <title>SEE ALSO</title>

    <para>ag++ (1), g++ (1).</para>

    <para>
      Again: for full reference on AspectC++ usage, see AspectC++
      documentation. It can be obtained from
      <emphasis>http://www.aspectc.org</emphasis>
    </para>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &lt;&dhemail;&gt; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 any 
	  later version published by the Free Software Foundation.
    </para>
	<para>
	  On Debian systems, the complete text of the GNU General Public
	  License can be found in /usr/share/common-licenses/GPL.
	</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


