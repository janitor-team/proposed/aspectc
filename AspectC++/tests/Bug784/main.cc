#include <iostream>

namespace namespace1 {
    attribute attr();
}

namespace namespace2 {
    attribute attr();
}

aspect testAspect {
    advice execution(namespace1::attr()) : after() {
      std::cout << "namespace1" << std::endl;
    }

    advice execution(namespace2::attr()) : after() {
        std::cout << "namespace2" << std::endl;
    }
};

struct Test {
    [[namespace1::attr]]
    static void print() {
        std::cout << "Expected: namespace1\n";
        std::cout << "Actual:   ";
    }
};

int main() {
    Test::print();
}
