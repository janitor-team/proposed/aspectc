// Test whether qualified names for extern-"C" type-def types
// inside namespaces are generated correctly.
// If it compiles it is fine.
namespace NS {
  extern "C" {
    typedef struct {
      int i;
    } param;
    void c_func(param *p) {
      return;
    }
    
    struct param2 {
      int i;
    };
    void c_func2(param2 *p) {
      return;
    }
  }
  
  extern "C"
  typedef struct {
    int i;
  } param3;
  void func3(param3 *p) {
    return;
  }
  
  extern "C"  
  struct param4 {
    int i;
  };
  void func4(param4 *p) {
    return;
  }
}

int main() {
  NS::c_func(0);
  NS::c_func2(0);
  NS::func3(0);
  NS::func4(0);
  return 0;
} 

aspect A {
  advice call("% ...::%(...)") : before() { }
};
